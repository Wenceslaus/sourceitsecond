package com.sourceitexamplefragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by wenceslaus on 27.09.16.
 */
public class RightFragment extends Fragment implements ITextChangeListener {

    public static final String EXTRA_COLOR = "extra_color";

    private int bgColor = Color.YELLOW;

    private View root;
    private TextView text;

    public static RightFragment newInstance(int color) {
        RightFragment fragment = new RightFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_COLOR, color);
        fragment.setArguments(args);
        return fragment;
    }

    public static RightFragment newInstance() {
        return new RightFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(EXTRA_COLOR)) {
            bgColor = getArguments().getInt(EXTRA_COLOR);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.right_fragment, container, false);
        root.setBackgroundColor(bgColor);
        text = (TextView) root.findViewById(R.id.label);
        return root;
    }

    @Override
    public void colorChange(int color, String text) {
        setColor(color, text);
    }

    public void setColor(int color, String name) {
        root.setBackgroundColor(color);
        text.setText(name);
    }
}
