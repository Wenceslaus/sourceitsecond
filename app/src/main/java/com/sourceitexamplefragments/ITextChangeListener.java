package com.sourceitexamplefragments;

/**
 * Created by wenceslaus on 27.09.16.
 */
public interface ITextChangeListener {
    void colorChange(int color, String text);
}
