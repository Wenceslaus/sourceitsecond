package com.sourceitexamplefragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

/**
 * Created by wenceslaus on 27.09.16.
 */
public class MainActivity extends FragmentActivity {

    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == 1) {
                findViewById(R.id.left_container).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.right_container).setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.left_container).setVisibility(View.INVISIBLE);//делаем невидимым контейнер
        findViewById(R.id.right_container).setVisibility(View.INVISIBLE);
        h.sendEmptyMessageDelayed(1, 1000);//показываем контейнеры с задержкой
        h.sendEmptyMessageDelayed(2, 1500);

        FragmentManager fm = getSupportFragmentManager();//получаем менеджер
        FragmentTransaction ft = fm.beginTransaction();//начинаем транзакцию
        //создаем фрагменты
        LeftFragment leftFragment = LeftFragment.newInstance();
        RightFragment rightFragment = RightFragment.newInstance();
        leftFragment.setTextChangeListener(rightFragment);//устанавливаем слушатель события, реагирующий на нажатие кнопки 3
        //добавляем фрагменты в контейтер, указывая в качестве тэга название фрагмента
        ft.add(R.id.left_container, leftFragment, LeftFragment.class.getSimpleName());
        ft.add(R.id.right_container, rightFragment, RightFragment.class.getSimpleName());
        ft.commit();//завершаем транзакцию, принимая все изменения
    }

    public void setColorToTheRightSide(int color, String name) {
        RightFragment fragment = (RightFragment) getSupportFragmentManager().findFragmentByTag(RightFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.setColor(color, name);
        }
    }

}
