package com.sourceitexamplefragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by wenceslaus on 27.09.16.
 */
public class LeftFragment extends Fragment {

    ITextChangeListener listener;

    public static LeftFragment newInstance() {
        return new LeftFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_fragment, container, false);

        //обращаемся к новосозданной view для получения кнопок
        Button actionBlue = (Button) view.findViewById(R.id.action_blue);
        Button actionGreen = (Button) view.findViewById(R.id.action_green);
        Button actionRed = (Button) view.findViewById(R.id.action_red);
        Button actionNewFragment = (Button) view.findViewById(R.id.action_new_fragment);

        //обновляем наш фрагмент через activity
        actionBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setColorToTheRightSide(Color.BLUE, getString(R.string.blue));
            }
        });

        //обновляем наш фрагмент из фрагмента
        actionGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RightFragment fragment = (RightFragment) getActivity().getSupportFragmentManager().findFragmentByTag(RightFragment.class.getSimpleName());
                if (fragment != null) {
                    fragment.setColor(Color.GREEN, getString(R.string.green));
                }
            }
        });

        //обновляем наш фрагмент с помощью интерфейса, который связывает два фрагмента
        actionRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.colorChange(Color.RED, getString(R.string.red));
            }
        });

        //замещаем наш фрагмент новым фрагментом
        actionNewFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                //обновляем слушатель, иначе кнопка red работать не будет
                RightFragment rightFragment = RightFragment.newInstance();
                LeftFragment leftFragment = (LeftFragment) getActivity().getSupportFragmentManager().findFragmentByTag(LeftFragment.class.getSimpleName());
                if (leftFragment != null) {
                    leftFragment.setTextChangeListener(rightFragment);
                }
                ft.replace(R.id.right_container, rightFragment, RightFragment.class.getSimpleName());
                ft.commit();
            }
        });


        return view;
    }

    public void setTextChangeListener(ITextChangeListener listener) {
        this.listener = listener;
    }

}
